Program stworzony przez
Wojciech Wilimowski
Jakub Krzowski


Program ten umożliwia przeszukanie podanej strony internetowej w poszukiwaniu obrazu w którego opisie znajduje się podane słowo oraz sprawdzenie czy temperatura w podanym mieście przekroczyła podaną wartość. Jeśli te warunki zostały spełnione program pozwala na wykonanie jednej z dwóch czynności
1. Wysłanie wiadomości na podany adres e-mail
2. Wyświetlenia znalezionego obrazu lub informacji o pogodzie na ekranie

W programie wykorzystano HtmlAgilityPack w celu przeszukiwania zawartości strony. Informacje o pogodzie pobierane są za pomocą API udostępnionego przez stronę http://openweathermap.org/. Interaktywna mapa wyświetlana w okienku informacji pogody została zrealizowana przy pomocy API Google Maps. Wszystkie zadania do wykonania przez program wyświetlane są w listboksie i ich wykonanie rozpoczyna się po wciśnięciu przycisku GO. Zadania dodawane do listy zapisywane są również do bazy danych i z tej bazy odczytywane są podczas uruchamiania programu. Podłączenie bazy danych wykonano przy pomocy Entity Framework.