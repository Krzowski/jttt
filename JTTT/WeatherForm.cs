﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows.Forms;
using JTTT.Misc;

namespace JTTT
{
    public partial class WeatherForm : Form
    {
        private CompleteWeather _weather;
        private string mapUrl;
        private int zoom = 8;
        private double offx = 0;
        private double offy = 0;
        private double _panConstant = 0.05;

        public WeatherForm(CompleteWeather w)
        {
            InitializeComponent();
            _weather = w;
            thermoBox.Text = _weather.Main.Temp -273.15 + "°C";
            windBox.Text = _weather.Wind.Speed + " km/h";
            mokroBox.Text = _weather.Main.Humidity + " %";
            weatherIconBox.ImageLocation = "http://openweathermap.org/img/w/" + _weather.Weather[0].Icon + ".png";
            reloadMap();
        }

        private void reloadMap()
        {
            mapUrl = "http://maps.google.com/maps/api/staticmap?center="
                + (_weather.Coord.Lat + offy).ToString(CultureInfo.CreateSpecificCulture("en-GB")) + ","
                + (_weather.Coord.Lon + offx).ToString(CultureInfo.CreateSpecificCulture("en-GB"))
                + "&zoom=" + zoom + "&size=" + mapBox.Size.Width + "x" + mapBox.Size.Height + "&maptype=roadmap&sensor=false";
            mapBox.ImageLocation = mapUrl;
        }

        private void WeatherForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            weatherIconBox.Image.Dispose();
            weatherIconBox.Dispose();
            mapBox.Image.Dispose();
            mapBox.Dispose();
        }


        private void zoomInButton_Click(object sender, EventArgs e)
        {
            if(zoom < 20) zoom++;
            reloadMap();
        }

        private void zoomOutButton_Click(object sender, EventArgs e)
        {
            if (zoom > 1) zoom--;
            reloadMap();
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            offy += _panConstant / zoom;
            reloadMap();
        }

        private void rightButton_Click(object sender, EventArgs e)
        {
            offx += _panConstant / zoom;
            reloadMap();
        }

        private void centerButton_Click(object sender, EventArgs e)
        {
            offx = 0;
            offy = 0;
            reloadMap();
        }

        private void leftButton_Click(object sender, EventArgs e)
        {
            offx -= _panConstant / zoom;
            reloadMap();
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            offy -= _panConstant / zoom;
            reloadMap();
        }
    }
}
