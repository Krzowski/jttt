﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    static class Progress
    {
        static ProgressBar progressBar;
        static Label label;
        private static int current;
        private static int total;
        private static int successful;
        public static void SetProgressBar(ProgressBar progressBarX)
        {
            progressBar = progressBarX;
            progressBar.Minimum = 0;
            progressBar.Step = 1;
        }
        public static void Reset()
        {
            total = 1;
            current = 0;
            successful = 0;
            Update();
        }
        public static void Increment()
        {
            current++;
            Update();
        }
        public static void Success()
        {
            successful++;
        }
        public static void SetTotal(int x)
        {
            total = x;
        }
        public static void SetCurrent(int x)
        {
            current = x;
        }
        public static void Update()
        {
            progressBar.Maximum = total;
            progressBar.Value = current;
            label.Text = String.Format("Wykonano {0} z {1}, sukcesy: {2}", current, total, successful);
            label.Update();
        }
        public static void SetActive(bool x)
        {
            progressBar.Visible = x;
            label.Visible = x;
        }
        public static void SetLabel(Label labelX)
        {
            label = labelX;
        }
    }
}
