﻿namespace JTTT
{
    public static class SendingController
    {
        static System.Windows.Forms.Label _label;
        public static void SetLabel(System.Windows.Forms.Label label)
        {
            _label = label;
        }

        public static void SetActive(bool x)
        {
            _label.Text = x ? "Wysylam..." : "";
        }
        
    }
}
