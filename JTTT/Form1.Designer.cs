﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urlBox = new System.Windows.Forms.TextBox();
            this.wordBox = new System.Windows.Forms.TextBox();
            this.EmailBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.sendingLabel = new System.Windows.Forms.Label();
            this.thenListBox = new System.Windows.Forms.ListBox();
            this.listAddButton = new System.Windows.Forms.Button();
            this.listClearButton = new System.Windows.Forms.Button();
            this.listRemoveButton = new System.Windows.Forms.Button();
            this.goButton = new System.Windows.Forms.Button();
            this.Serialize_Button = new System.Windows.Forms.Button();
            this.Deserialize_Button = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.cityBox = new System.Windows.Forms.TextBox();
            this.thermoBox = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.WindowTaskButton = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // urlBox
            // 
            this.urlBox.Location = new System.Drawing.Point(118, 6);
            this.urlBox.Name = "urlBox";
            this.urlBox.Size = new System.Drawing.Size(226, 20);
            this.urlBox.TabIndex = 1;
            this.urlBox.Text = "http://demotywatory.pl/";
            // 
            // wordBox
            // 
            this.wordBox.Location = new System.Drawing.Point(118, 32);
            this.wordBox.Name = "wordBox";
            this.wordBox.Size = new System.Drawing.Size(226, 20);
            this.wordBox.TabIndex = 2;
            // 
            // EmailBox
            // 
            this.EmailBox.Location = new System.Drawing.Point(112, 6);
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.Size = new System.Drawing.Size(197, 20);
            this.EmailBox.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.textBox1.Location = new System.Drawing.Point(6, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(106, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Gdzie mam szukać?";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.textBox2.Location = new System.Drawing.Point(6, 32);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(106, 20);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "Czego mam szukać?";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.LawnGreen;
            this.textBox4.Location = new System.Drawing.Point(1, 6);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(105, 20);
            this.textBox4.TabIndex = 8;
            this.textBox4.Text = "Gdzie mam wysłać?";
            // 
            // sendingLabel
            // 
            this.sendingLabel.AutoSize = true;
            this.sendingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sendingLabel.Location = new System.Drawing.Point(17, 228);
            this.sendingLabel.Name = "sendingLabel";
            this.sendingLabel.Size = new System.Drawing.Size(86, 17);
            this.sendingLabel.TabIndex = 9;
            this.sendingLabel.Text = "Wysylam...";
            // 
            // thenListBox
            // 
            this.thenListBox.FormattingEnabled = true;
            this.thenListBox.Location = new System.Drawing.Point(367, 8);
            this.thenListBox.Name = "thenListBox";
            this.thenListBox.Size = new System.Drawing.Size(590, 173);
            this.thenListBox.TabIndex = 10;
            // 
            // listAddButton
            // 
            this.listAddButton.Location = new System.Drawing.Point(6, 28);
            this.listAddButton.Name = "listAddButton";
            this.listAddButton.Size = new System.Drawing.Size(75, 23);
            this.listAddButton.TabIndex = 11;
            this.listAddButton.Text = "Dodaj";
            this.listAddButton.UseVisualStyleBackColor = true;
            this.listAddButton.Click += new System.EventHandler(this.listAddButton_Click);
            // 
            // listClearButton
            // 
            this.listClearButton.Location = new System.Drawing.Point(610, 190);
            this.listClearButton.Name = "listClearButton";
            this.listClearButton.Size = new System.Drawing.Size(75, 23);
            this.listClearButton.TabIndex = 12;
            this.listClearButton.Text = "Wyczyść";
            this.listClearButton.UseVisualStyleBackColor = true;
            this.listClearButton.Click += new System.EventHandler(this.listClearButton_Click);
            // 
            // listRemoveButton
            // 
            this.listRemoveButton.Location = new System.Drawing.Point(529, 190);
            this.listRemoveButton.Name = "listRemoveButton";
            this.listRemoveButton.Size = new System.Drawing.Size(75, 23);
            this.listRemoveButton.TabIndex = 13;
            this.listRemoveButton.Text = "Usuń";
            this.listRemoveButton.UseVisualStyleBackColor = true;
            this.listRemoveButton.Click += new System.EventHandler(this.listRemoveButton_Click);
            // 
            // goButton
            // 
            this.goButton.BackColor = System.Drawing.Color.Crimson;
            this.goButton.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.goButton.FlatAppearance.BorderSize = 5;
            this.goButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.goButton.Location = new System.Drawing.Point(367, 219);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(318, 52);
            this.goButton.TabIndex = 14;
            this.goButton.Text = "Go";
            this.goButton.UseVisualStyleBackColor = false;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // Serialize_Button
            // 
            this.Serialize_Button.Location = new System.Drawing.Point(367, 190);
            this.Serialize_Button.Name = "Serialize_Button";
            this.Serialize_Button.Size = new System.Drawing.Size(75, 23);
            this.Serialize_Button.TabIndex = 15;
            this.Serialize_Button.Text = "Ser";
            this.Serialize_Button.UseVisualStyleBackColor = true;
            this.Serialize_Button.Click += new System.EventHandler(this.Serialize_Button_Click);
            // 
            // Deserialize_Button
            // 
            this.Deserialize_Button.Location = new System.Drawing.Point(448, 190);
            this.Deserialize_Button.Name = "Deserialize_Button";
            this.Deserialize_Button.Size = new System.Drawing.Size(75, 23);
            this.Deserialize_Button.TabIndex = 16;
            this.Deserialize_Button.Text = "DeSer";
            this.Deserialize_Button.UseVisualStyleBackColor = true;
            this.Deserialize_Button.Click += new System.EventHandler(this.Deserialize_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(20, 248);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(326, 23);
            this.progressBar.TabIndex = 17;
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "";
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(338, 113);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightCyan;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.urlBox);
            this.tabPage1.Controls.Add(this.wordBox);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(330, 87);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Demotywatory";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LemonChiffon;
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.cityBox);
            this.tabPage2.Controls.Add(this.thermoBox);
            this.tabPage2.Controls.Add(this.textBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(330, 87);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pogoda";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.DarkOrange;
            this.textBox3.Location = new System.Drawing.Point(5, 32);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(106, 20);
            this.textBox3.TabIndex = 14;
            this.textBox3.Text = "A w jakim mieście?";
            // 
            // cityBox
            // 
            this.cityBox.Location = new System.Drawing.Point(117, 32);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(196, 20);
            this.cityBox.TabIndex = 12;
            this.cityBox.Text = "Wrocław";
            // 
            // thermoBox
            // 
            this.thermoBox.Location = new System.Drawing.Point(116, 6);
            this.thermoBox.Name = "thermoBox";
            this.thermoBox.Size = new System.Drawing.Size(197, 20);
            this.thermoBox.TabIndex = 13;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.DarkOrange;
            this.textBox7.Location = new System.Drawing.Point(5, 6);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(106, 20);
            this.textBox7.TabIndex = 15;
            this.textBox7.Text = "Jak ciepło ma być?";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(12, 131);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(338, 82);
            this.tabControl2.TabIndex = 19;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Honeydew;
            this.tabPage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage3.Controls.Add(this.EmailBox);
            this.tabPage3.Controls.Add(this.textBox4);
            this.tabPage3.Controls.Add(this.listAddButton);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(330, 56);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "E-mail";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightSalmon;
            this.tabPage4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage4.Controls.Add(this.WindowTaskButton);
            this.tabPage4.Controls.Add(this.textBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(330, 56);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Okienko";
            // 
            // WindowTaskButton
            // 
            this.WindowTaskButton.Location = new System.Drawing.Point(6, 28);
            this.WindowTaskButton.Name = "WindowTaskButton";
            this.WindowTaskButton.Size = new System.Drawing.Size(75, 23);
            this.WindowTaskButton.TabIndex = 17;
            this.WindowTaskButton.Text = "Dodaj";
            this.WindowTaskButton.UseVisualStyleBackColor = true;
            this.WindowTaskButton.Click += new System.EventHandler(this.WindowTaskButton_Click);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Red;
            this.textBox5.Location = new System.Drawing.Point(6, 6);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(303, 20);
            this.textBox5.TabIndex = 16;
            this.textBox5.Text = "Otworzę Ci okienko jak pani w dziekanacie o 8 rano";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1009, 281);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.Deserialize_Button);
            this.Controls.Add(this.Serialize_Button);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.listRemoveButton);
            this.Controls.Add(this.listClearButton);
            this.Controls.Add(this.thenListBox);
            this.Controls.Add(this.sendingLabel);
            this.Name = "Form1";
            this.Text = "Szeryf Internetu";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox urlBox;
        private System.Windows.Forms.TextBox wordBox;
        private System.Windows.Forms.TextBox EmailBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label sendingLabel;
        private System.Windows.Forms.ListBox thenListBox;
        private System.Windows.Forms.Button listAddButton;
        private System.Windows.Forms.Button listClearButton;
        private System.Windows.Forms.Button listRemoveButton;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.Button Serialize_Button;
        private System.Windows.Forms.Button Deserialize_Button;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox thermoBox;
        private System.Windows.Forms.TextBox cityBox;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button WindowTaskButton;
    }
}

