﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable]
    public class JtttTask
    {
        public int JtttTaskId { get; set; }

        public virtual void Execute(JtttCondition c)
        {
            MessageBox.Show("Task Execute -To nie powinno sie zdarzyc");
        }

        public virtual void More(JtttCondition c)
        {
            MessageBox.Show("Task More -To nie powinno sie zdarzyc");
        }
    }

    [Serializable]
    
    internal class JtttMailTask : JtttTask
    {
        public string _address { get; set; }
        public string _filepath { get; set; }

        public int conditionType { get; set; }

        public JtttMailTask()
        {

        }
        public JtttMailTask(string address)
        {
            _address = address;
        }

        public override void Execute(JtttCondition c)
        {
            switch (c.conditionType)
            {
                case 1:
                    var imageCondition = c as JtttImageCondition;
                    var filename = MyStaticHtml.DownloadImage(imageCondition._found);
                    MyStaticMailer.SendFile(_address, filename);
                    break;
                case 2:
                    var weatherCondition = c as JtttWeatherCondition;
                    if (weatherCondition != null) MyStaticMailer.SendText(_address, "Ponad "+weatherCondition.target +" stopni w miescie " + weatherCondition.City);
                    break;
            }
            
        }

        public override string ToString()
        {
            return "wyślij maila na adres " + _address;
        }

        
    }

    internal class JTTTWindowTask : JtttTask
    {

        public string _filepath { get; set; }
        public int conditionType { get; set; }

        public JTTTWindowTask()
        {

        }

        public JTTTWindowTask(string filepath)
        {
            _filepath = filepath;

        }


        public override void Execute(JtttCondition c)
        {
            switch (c.conditionType)
            {
                case 1:
                    var imageCondition = c as JtttImageCondition;
                    if (imageCondition != null) _filepath = imageCondition.GetFound();
                    var filename = MyStaticHtml.DownloadImage(_filepath);               
                    StaticDisplayer.DisplayImage(filename);     
                    break;
                case 2:
                    var weatherCondition = c as JtttWeatherCondition;
                    if (weatherCondition != null) StaticDisplayer.DisplayWeather(weatherCondition.Weather);
                    break;

            }

        }
          
        public override string ToString()
        {
            return "wyswietl ten obrazek ";
        }
    }
}