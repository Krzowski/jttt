﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Forms;
using JTTT.Misc;

namespace JTTT
{
    [Serializable]
    public class JtttCondition
    {
        
        public int JtttConditionId { get; set; }
        public int conditionType { get; set; }
        public virtual bool Check()
        {
            MessageBox.Show("Condition Check - To nie powinno sie zdarzyc");
            return false;
        }

    }
    [Serializable]
    
    internal class JtttImageCondition : JtttCondition
    {
        public string _found { get; set; }

        public string _keyword { get; set; }
        public string _website { get; set; }


        public JtttImageCondition()
        {
            conditionType = 1;
        }
        public JtttImageCondition(string website, string keyword)
        {
            conditionType = 1;
            _website = website;
            _keyword = keyword;
        }

        public override bool Check()
        {
            MyStaticHtml.SetUrl(_website);
            _found = MyStaticHtml.GetImageUrl(_keyword);
            if (_found == "Not found") return false;
            return true;
        }

        public string GetFound()
        {
            return _found;
        }

        public override string ToString()
        {
            return "na stronie " + _website + " jest obrazek z podpisem " + _keyword;
        }
    }

    internal class JtttWeatherCondition : JtttCondition
    {
        public string City { get; set; }
        public int target { get; set; }
        [NotMapped]
        public CompleteWeather Weather { get; set; }

        public JtttWeatherCondition()
        {
            conditionType = 2;
        }

        public JtttWeatherCondition(string city, int targetti)
        {
            conditionType = 2;
            City = city;
            target = targetti;
        }

        public override bool Check()
        {
            Weather = Weatherman.GetWeather(City);
            return Weather.Main.Temp >= target;
        }
        

        public override string ToString()
        {
            return "temperatura w " + City + " wynosi co najmniej " + target;
        }

        public string WeatherMessage()
        {
            return "Temperatura w " + City + " przekroczyła " + target + " stopni!";
        }
        
    }
}