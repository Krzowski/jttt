﻿using System;

namespace JTTT
{
    [Serializable]
    public class ConditionTask
    {
        public int ConditionTaskId { get; set; }


        public int? JtttConditionId { get; set; }
        public int? JtttTaskId { get; set; }


        public virtual JtttCondition _jtttCondition { get; set; }
        public virtual JtttTask _jtttTask { get; set; }

        public ConditionTask()
        {

        }

        public ConditionTask(ConditionTask CT)
        {
            _jtttCondition = CT._jtttCondition;
            _jtttTask = CT._jtttTask;

        }

        public ConditionTask(JtttCondition condition, JtttTask task)
        {
            _jtttCondition = condition;
            _jtttTask = task;

        }

        public bool Run()
        {
            if (!_jtttCondition.Check()) return false;
            Progress.Success();
            _jtttTask.Execute(_jtttCondition);
            return true;
        }

        public override string ToString()
        {
            return "Jeśli " + _jtttCondition + " to " + _jtttTask;
        }

        public bool DbAdd()
        {
            
            using (var ctx = new ConditionTaskDbContext())
            {
                ctx.ConditionTasks.Add(this);
                ctx.Conditions.Add(_jtttCondition);
                ctx.Tasks.Add(_jtttTask);
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
