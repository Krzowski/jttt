namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class cos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JtttConditions",
                c => new
                    {
                        JtttConditionId = c.Int(nullable: false, identity: true),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.JtttConditionId);
            
            CreateTable(
                "dbo.ConditionTasks",
                c => new
                    {
                        ConditionTaskId = c.Int(nullable: false, identity: true),
                        JtttConditionId = c.Int(),
                        JtttTaskId = c.Int(),
                    })
                .PrimaryKey(t => t.ConditionTaskId)
                .ForeignKey("dbo.JtttConditions", t => t.JtttConditionId)
                .ForeignKey("dbo.JtttTasks", t => t.JtttTaskId)
                .Index(t => t.JtttConditionId)
                .Index(t => t.JtttTaskId);
            
            CreateTable(
                "dbo.JtttTasks",
                c => new
                    {
                        JtttTaskId = c.Int(nullable: false, identity: true),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.JtttTaskId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConditionTasks", "JtttTaskId", "dbo.JtttTasks");
            DropForeignKey("dbo.ConditionTasks", "JtttConditionId", "dbo.JtttConditions");
            DropIndex("dbo.ConditionTasks", new[] { "JtttTaskId" });
            DropIndex("dbo.ConditionTasks", new[] { "JtttConditionId" });
            DropTable("dbo.JtttTasks");
            DropTable("dbo.ConditionTasks");
            DropTable("dbo.JtttConditions");
        }
    }
}
