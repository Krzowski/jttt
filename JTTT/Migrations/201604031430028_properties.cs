namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class properties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageCondition", "_found", c => c.String());
            AddColumn("dbo.ImageCondition", "_keyword", c => c.String());
            AddColumn("dbo.ImageCondition", "_website", c => c.String());
            AddColumn("dbo.ImageTasks", "_address", c => c.String());
            AddColumn("dbo.ImageTasks", "_filepath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ImageTasks", "_filepath");
            DropColumn("dbo.ImageTasks", "_address");
            DropColumn("dbo.ImageCondition", "_website");
            DropColumn("dbo.ImageCondition", "_keyword");
            DropColumn("dbo.ImageCondition", "_found");
        }
    }
}
