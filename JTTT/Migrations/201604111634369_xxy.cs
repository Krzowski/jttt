namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class xxy : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CompleteWeathers", "Sys_Id", "dbo.Sys");
            DropForeignKey("dbo.Weathers", "CompleteWeather_Id", "dbo.CompleteWeathers");
            DropForeignKey("dbo.JtttConditions", "Weather_Id", "dbo.CompleteWeathers");
            DropIndex("dbo.JtttConditions", new[] { "Weather_Id" });
            DropIndex("dbo.CompleteWeathers", new[] { "Sys_Id" });
            DropIndex("dbo.Weathers", new[] { "CompleteWeather_Id" });
            DropColumn("dbo.JtttConditions", "Weather_Id");
            DropTable("dbo.CompleteWeathers");
            DropTable("dbo.Sys");
            DropTable("dbo.Weathers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Weathers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Main = c.String(),
                        Description = c.String(),
                        Icon = c.String(),
                        CompleteWeather_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Message = c.Double(nullable: false),
                        Country = c.String(),
                        Sunrise = c.Int(nullable: false),
                        Sunset = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CompleteWeathers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Coord_Lon = c.Double(nullable: false),
                        Coord_Lat = c.Double(nullable: false),
                        Base = c.String(),
                        Main_Temp = c.Double(nullable: false),
                        Main_Pressure = c.Int(nullable: false),
                        Main_Humidity = c.Int(nullable: false),
                        Main_TempMin = c.Double(nullable: false),
                        Main_TempMax = c.Double(nullable: false),
                        Wind_Speed = c.Double(nullable: false),
                        Clouds_All = c.Int(nullable: false),
                        Dt = c.Int(nullable: false),
                        Name = c.String(),
                        Cod = c.Int(nullable: false),
                        Sys_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.JtttConditions", "Weather_Id", c => c.Int());
            CreateIndex("dbo.Weathers", "CompleteWeather_Id");
            CreateIndex("dbo.CompleteWeathers", "Sys_Id");
            CreateIndex("dbo.JtttConditions", "Weather_Id");
            AddForeignKey("dbo.JtttConditions", "Weather_Id", "dbo.CompleteWeathers", "Id");
            AddForeignKey("dbo.Weathers", "CompleteWeather_Id", "dbo.CompleteWeathers", "Id");
            AddForeignKey("dbo.CompleteWeathers", "Sys_Id", "dbo.Sys", "Id");
        }
    }
}
