namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class yyy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JtttTasks", "_filepath1", c => c.String());
            AddColumn("dbo.JtttTasks", "conditionType1", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JtttTasks", "conditionType1");
            DropColumn("dbo.JtttTasks", "_filepath1");
        }
    }
}
