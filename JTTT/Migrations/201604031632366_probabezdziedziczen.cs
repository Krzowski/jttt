namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class probabezdziedziczen : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ImageCondition", "JtttConditionId", "dbo.JtttConditions");
            DropForeignKey("dbo.ImageTasks", "JtttTaskId", "dbo.JtttTasks");
            DropIndex("dbo.ImageCondition", new[] { "JtttConditionId" });
            DropIndex("dbo.ImageTasks", new[] { "JtttTaskId" });
            AddColumn("dbo.JtttConditions", "_found", c => c.String());
            AddColumn("dbo.JtttConditions", "_keyword", c => c.String());
            AddColumn("dbo.JtttConditions", "_website", c => c.String());
            AddColumn("dbo.JtttConditions", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.JtttTasks", "_address", c => c.String());
            AddColumn("dbo.JtttTasks", "_filepath", c => c.String());
            AddColumn("dbo.JtttTasks", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropTable("dbo.ImageCondition");
            DropTable("dbo.ImageTasks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ImageTasks",
                c => new
                    {
                        JtttTaskId = c.Int(nullable: false),
                        _address = c.String(),
                        _filepath = c.String(),
                    })
                .PrimaryKey(t => t.JtttTaskId);
            
            CreateTable(
                "dbo.ImageCondition",
                c => new
                    {
                        JtttConditionId = c.Int(nullable: false),
                        _found = c.String(),
                        _keyword = c.String(),
                        _website = c.String(),
                    })
                .PrimaryKey(t => t.JtttConditionId);
            
            DropColumn("dbo.JtttTasks", "Discriminator");
            DropColumn("dbo.JtttTasks", "_filepath");
            DropColumn("dbo.JtttTasks", "_address");
            DropColumn("dbo.JtttConditions", "Discriminator");
            DropColumn("dbo.JtttConditions", "_website");
            DropColumn("dbo.JtttConditions", "_keyword");
            DropColumn("dbo.JtttConditions", "_found");
            CreateIndex("dbo.ImageTasks", "JtttTaskId");
            CreateIndex("dbo.ImageCondition", "JtttConditionId");
            AddForeignKey("dbo.ImageTasks", "JtttTaskId", "dbo.JtttTasks", "JtttTaskId");
            AddForeignKey("dbo.ImageCondition", "JtttConditionId", "dbo.JtttConditions", "JtttConditionId");
        }
    }
}
