namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class weather : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JtttConditions", "conditionType", c => c.Int(nullable: false));
            AddColumn("dbo.JtttConditions", "City", c => c.String());
            AddColumn("dbo.JtttConditions", "target", c => c.Int());
            AddColumn("dbo.JtttTasks", "conditionType", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JtttTasks", "conditionType");
            DropColumn("dbo.JtttConditions", "target");
            DropColumn("dbo.JtttConditions", "City");
            DropColumn("dbo.JtttConditions", "conditionType");
        }
    }
}
