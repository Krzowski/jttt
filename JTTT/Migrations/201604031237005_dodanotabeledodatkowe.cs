namespace JTTT.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class dodanotabeledodatkowe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImageCondition",
                c => new
                    {
                        JtttConditionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.JtttConditionId)
                .ForeignKey("dbo.JtttConditions", t => t.JtttConditionId)
                .Index(t => t.JtttConditionId);
            
            CreateTable(
                "dbo.ImageTasks",
                c => new
                    {
                        JtttTaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.JtttTaskId)
                .ForeignKey("dbo.JtttTasks", t => t.JtttTaskId)
                .Index(t => t.JtttTaskId);
            
            DropColumn("dbo.JtttConditions", "Discriminator");
            DropColumn("dbo.JtttTasks", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.JtttTasks", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.JtttConditions", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.ImageTasks", "JtttTaskId", "dbo.JtttTasks");
            DropForeignKey("dbo.ImageCondition", "JtttConditionId", "dbo.JtttConditions");
            DropIndex("dbo.ImageTasks", new[] { "JtttTaskId" });
            DropIndex("dbo.ImageCondition", new[] { "JtttConditionId" });
            DropTable("dbo.ImageTasks");
            DropTable("dbo.ImageCondition");
        }
    }
}
