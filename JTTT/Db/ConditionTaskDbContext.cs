﻿using System.Data.Entity;

namespace JTTT
{
    public class ConditionTaskDbContext : DbContext
    {
        public ConditionTaskDbContext() : base("AllAboutThatBase")
        {


        }

        public DbSet<ConditionTask> ConditionTasks { get; set; }
        public DbSet<JtttCondition> Conditions { get; set; }
        public DbSet<JtttTask> Tasks { get; set; }
    }
}
