﻿using System.ComponentModel;

namespace JTTT
{
    internal static class DataBaseReader
    {

        public static BindingList<ConditionTask> ReadFromdataBase()
        {

             BindingList<ConditionTask> thenList = new BindingList<ConditionTask>();
           
            using (var ctx = new ConditionTaskDbContext())
            {

                foreach (var CT in ctx.ConditionTasks)
                { 
                       
                       var conditionTask = new ConditionTask(CT._jtttCondition,CT._jtttTask);                       
                       thenList.Add(conditionTask);
                                      
                }
            }


            return thenList;
        }



    }
}
