﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace JTTT
{
    

    public partial class Form1 : Form
    {
        private readonly BindingList<ConditionTask> thenList;
        public Form1()
        {
            InitializeComponent();
            
            MyStaticMailer.SetMail("poczta.o2.pl", "browary.kielbasa@o2.pl", "iinnetowary");
            MyStaticMailer.Connect();
            SendingController.SetLabel(this.sendingLabel);
            SendingController.SetActive(false);

            thenList = DataBaseReader.ReadFromdataBase();
            thenListBox.DataSource=thenList;
            Progress.SetProgressBar(progressBar);
            Progress.SetLabel(sendingLabel);
            Progress.SetActive(false);
            Logger.Log("New session opened");

            


        }

        private void listAddButton_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
                      AddMailFromUrlTask();   
            else
                AddMailWithWeather();
        }

        private void AddMailWithWeather()
        {
            var thermo = int.Parse(thermoBox.Text);
            var city = cityBox.Text;
            var email = EmailBox.Text;
            var conditionTask = new ConditionTask(new JtttWeatherCondition(city, thermo), new JtttMailTask(email));
            thenList.Add(conditionTask);
            conditionTask.DbAdd();
            cityBox.Text = "";
        }

        private void AddMailFromUrlTask()
        {
            var url = urlBox.Text;
            var word = wordBox.Text;
            var email = EmailBox.Text;
            var conditionTask = new ConditionTask(new JtttImageCondition(url, word), new JtttMailTask(email));
            thenList.Add(conditionTask);
            conditionTask.DbAdd();
            wordBox.Text = "";
        }

        private void listRemoveButton_Click(object sender, EventArgs e)
        {
          
            thenList.RemoveAt(thenListBox.SelectedIndex);
        
        }

        private void listClearButton_Click(object sender, EventArgs e)
        {
            thenList.Clear();
        }


        private void goButton_Click(object sender, EventArgs e)
        {
            Progress.Reset();
            Progress.SetTotal(thenList.Count);
            Progress.SetActive(true);
            foreach (var then in thenList)
            {
                then.Run();
                Progress.Increment();
                Thread.Sleep(1);
            }
            MessageBox.Show("All tasks complete");
            Progress.SetActive(false);
        }

        private void Deserialize_Click(object sender, EventArgs e)
            {
            var tmp = Serialize.Deserialization("Ser.bin");
            if (tmp == null) throw new ArgumentNullException(nameof(tmp));
            foreach (var temp in tmp)
                thenList.Add(temp);
            }

        private void Serialize_Button_Click(object sender, EventArgs e)
        {
            Serialize.Serialization(thenList, "Ser.bin");
        }


        private void WindowTaskButton_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                AddWindowFromUrlTask();

            }

            else
            {
                AddWindowWithWeather();
            }


        }

        private void AddWindowWithWeather()
        {
            var thermo = int.Parse(thermoBox.Text);
            var city = cityBox.Text;
            var conditionTask = new ConditionTask(new JtttWeatherCondition(city, thermo), new JTTTWindowTask());
            thenList.Add(conditionTask);
            conditionTask.DbAdd();
            cityBox.Text = "";
        }

        private void AddWindowFromUrlTask()
        {
            var url = urlBox.Text;
            var word = wordBox.Text;
            var conditionTask = new ConditionTask(new JtttImageCondition(url, word), new JTTTWindowTask());
            thenList.Add(conditionTask);
            conditionTask.DbAdd();
            wordBox.Text = "";
        }
    }
}