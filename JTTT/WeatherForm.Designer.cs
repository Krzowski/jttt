﻿namespace JTTT
{
    partial class WeatherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.weatherIconBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.thermoBox = new System.Windows.Forms.TextBox();
            this.windBox = new System.Windows.Forms.TextBox();
            this.mokroBox = new System.Windows.Forms.TextBox();
            this.label2cityLabel = new System.Windows.Forms.Label();
            this.mapBox = new System.Windows.Forms.PictureBox();
            this.zoomInButton = new System.Windows.Forms.Button();
            this.zoomOutButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.centerButton = new System.Windows.Forms.Button();
            this.rightButton = new System.Windows.Forms.Button();
            this.leftButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.weatherIconBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapBox)).BeginInit();
            this.SuspendLayout();
            // 
            // weatherIconBox
            // 
            this.weatherIconBox.Location = new System.Drawing.Point(12, 239);
            this.weatherIconBox.Name = "weatherIconBox";
            this.weatherIconBox.Size = new System.Drawing.Size(65, 66);
            this.weatherIconBox.TabIndex = 0;
            this.weatherIconBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 242);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Temperatura:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Prędkość wiatru:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Wilgotność:";
            // 
            // thermoBox
            // 
            this.thermoBox.Location = new System.Drawing.Point(353, 239);
            this.thermoBox.Name = "thermoBox";
            this.thermoBox.ReadOnly = true;
            this.thermoBox.Size = new System.Drawing.Size(71, 20);
            this.thermoBox.TabIndex = 8;
            // 
            // windBox
            // 
            this.windBox.Location = new System.Drawing.Point(353, 262);
            this.windBox.Name = "windBox";
            this.windBox.ReadOnly = true;
            this.windBox.Size = new System.Drawing.Size(71, 20);
            this.windBox.TabIndex = 9;
            // 
            // mokroBox
            // 
            this.mokroBox.Location = new System.Drawing.Point(353, 285);
            this.mokroBox.Name = "mokroBox";
            this.mokroBox.ReadOnly = true;
            this.mokroBox.Size = new System.Drawing.Size(71, 20);
            this.mokroBox.TabIndex = 10;
            // 
            // label2cityLabel
            // 
            this.label2cityLabel.AutoSize = true;
            this.label2cityLabel.Location = new System.Drawing.Point(12, 262);
            this.label2cityLabel.Name = "label2cityLabel";
            this.label2cityLabel.Size = new System.Drawing.Size(0, 13);
            this.label2cityLabel.TabIndex = 12;
            // 
            // mapBox
            // 
            this.mapBox.Location = new System.Drawing.Point(12, 12);
            this.mapBox.Name = "mapBox";
            this.mapBox.Size = new System.Drawing.Size(412, 221);
            this.mapBox.TabIndex = 15;
            this.mapBox.TabStop = false;
            // 
            // zoomInButton
            // 
            this.zoomInButton.BackColor = System.Drawing.Color.Coral;
            this.zoomInButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zoomInButton.Location = new System.Drawing.Point(84, 240);
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(37, 35);
            this.zoomInButton.TabIndex = 16;
            this.zoomInButton.Text = "+";
            this.zoomInButton.UseVisualStyleBackColor = false;
            this.zoomInButton.Click += new System.EventHandler(this.zoomInButton_Click);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.BackColor = System.Drawing.Color.DarkTurquoise;
            this.zoomOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zoomOutButton.Location = new System.Drawing.Point(84, 274);
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(37, 35);
            this.zoomOutButton.TabIndex = 17;
            this.zoomOutButton.Text = "-";
            this.zoomOutButton.UseVisualStyleBackColor = false;
            this.zoomOutButton.Click += new System.EventHandler(this.zoomOutButton_Click);
            // 
            // upButton
            // 
            this.upButton.Location = new System.Drawing.Point(166, 242);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(20, 20);
            this.upButton.TabIndex = 18;
            this.upButton.Text = "^";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // downButton
            // 
            this.downButton.Location = new System.Drawing.Point(166, 289);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(20, 20);
            this.downButton.TabIndex = 19;
            this.downButton.Text = "v";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // centerButton
            // 
            this.centerButton.Location = new System.Drawing.Point(166, 265);
            this.centerButton.Name = "centerButton";
            this.centerButton.Size = new System.Drawing.Size(20, 20);
            this.centerButton.TabIndex = 20;
            this.centerButton.Text = "X";
            this.centerButton.UseVisualStyleBackColor = true;
            this.centerButton.Click += new System.EventHandler(this.centerButton_Click);
            // 
            // rightButton
            // 
            this.rightButton.Location = new System.Drawing.Point(192, 265);
            this.rightButton.Name = "rightButton";
            this.rightButton.Size = new System.Drawing.Size(20, 20);
            this.rightButton.TabIndex = 21;
            this.rightButton.Text = ">";
            this.rightButton.UseVisualStyleBackColor = true;
            this.rightButton.Click += new System.EventHandler(this.rightButton_Click);
            // 
            // leftButton
            // 
            this.leftButton.Location = new System.Drawing.Point(140, 265);
            this.leftButton.Name = "leftButton";
            this.leftButton.Size = new System.Drawing.Size(20, 20);
            this.leftButton.TabIndex = 22;
            this.leftButton.Text = "<";
            this.leftButton.UseVisualStyleBackColor = true;
            this.leftButton.Click += new System.EventHandler(this.leftButton_Click);
            // 
            // WeatherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(436, 317);
            this.Controls.Add(this.leftButton);
            this.Controls.Add(this.rightButton);
            this.Controls.Add(this.centerButton);
            this.Controls.Add(this.downButton);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.zoomOutButton);
            this.Controls.Add(this.zoomInButton);
            this.Controls.Add(this.mapBox);
            this.Controls.Add(this.label2cityLabel);
            this.Controls.Add(this.mokroBox);
            this.Controls.Add(this.windBox);
            this.Controls.Add(this.thermoBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.weatherIconBox);
            this.Name = "WeatherForm";
            this.Text = "WeatherForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WeatherForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.weatherIconBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox weatherIconBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox thermoBox;
        private System.Windows.Forms.TextBox windBox;
        private System.Windows.Forms.TextBox mokroBox;
        private System.Windows.Forms.Label label2cityLabel;
        private System.Windows.Forms.PictureBox mapBox;
        private System.Windows.Forms.Button zoomInButton;
        private System.Windows.Forms.Button zoomOutButton;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Button centerButton;
        private System.Windows.Forms.Button rightButton;
        private System.Windows.Forms.Button leftButton;
    }
}