﻿using System;
using System.IO;

namespace JTTT
{
    internal class Logger
    {
        public static void Log(string s)
        {
            var sw = new StreamWriter("log.txt", true);
            sw.WriteLine(DateTime.Now.ToLongTimeString() + " " + s);
            sw.Close();
        }
    }
}