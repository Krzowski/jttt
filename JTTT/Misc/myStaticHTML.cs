﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace JTTT
{
    internal static class MyStaticHtml
    {
        private static string _url;

        public static void SetUrl(string url)
        {
            _url = url;
        }

        public static string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = WebUtility.HtmlDecode(wc.DownloadString(_url));
                Logger.Log("Got Page HTML");
                return html;
            }
        }

        public static string DownloadImage(string filePath)
        {
            using (var client = new WebClient())
            {
                try
                {
                    var fileName = Path.GetFileName(filePath);
                    client.DownloadFile(filePath, fileName);
                    Logger.Log("Image Downloaded");
                    return fileName;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "ERROR!");
                    Logger.Log("ERROR!!!" + e);
                    return "";
                }
            }
        }

        public static string GetImageUrl(string word)
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");
            foreach (var node in nodes)
            {
                var x = node.GetAttributeValue("alt", "");
                var contains = x.IndexOf(word, StringComparison.OrdinalIgnoreCase) >= 0; 
                if (!contains) continue;
                var filePath = node.GetAttributeValue("src", "");
                if (!filePath.Contains("http")) 
                {
                    filePath = _url + filePath;
                }
                Logger.Log("Got image URL");
                return filePath;
            }
            Logger.Log("Not found");
            return "Not found";
        }

    }
}