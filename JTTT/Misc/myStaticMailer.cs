﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

namespace JTTT
{
    internal static class MyStaticMailer
    {
        private static SmtpClient _client;
        private static string _server;
        private static string _myMail;
        private static string _myPass;

        public static void SetMail(string server, string myMail, string myPass)
        {
            _server = server;
            _myMail = myMail;
            _myPass = myPass;
        }

        public static bool Connect()
        {
            try
            {
                _client = new SmtpClient(_server, 587)
                {
                    Credentials = new NetworkCredential(_myMail, _myPass),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Timeout = 10000
                };
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                Logger.Log(e.ToString());
                return false;
            }
        }

        public static bool SendFile(string address, string filename)
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            
            var myMail = new MailMessage(_myMail, address, "Obrazek od Szeryfa Internetu! [" + timeSpan.TotalSeconds.ToString() + "]",
                "Szeryf pozdrawia i życzy szerokości");
            var image = new Attachment(filename);
            myMail.Attachments.Add(image);
            
            SendingController.SetActive(true);
            try
            {
                _client.Send(myMail);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR!");
                Logger.Log(e.ToString());
                return false;
            }
            myMail.Attachments.Dispose();
            File.Delete(filename);
            SendingController.SetActive(false);
            Logger.Log("Image Sent");
            return true;
        }

        public static bool SendText(string address, string content)
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));

            var myMail = new MailMessage(_myMail, address, 
                "Wiadomość od Szeryfa Internetu! [" + timeSpan.TotalSeconds + "]",
                content);
            SendingController.SetActive(true);
            try
            {
                _client.Send(myMail);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR!");
                Logger.Log(e.ToString());
                return false;
            }
            SendingController.SetActive(false);
            Logger.Log("Message Sent");
            return true;
        }

    }
}