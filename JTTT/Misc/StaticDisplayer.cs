﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using JTTT.Misc;

namespace JTTT
{
    static class StaticDisplayer
    {
        public static void DisplayImage(string filename)
        {
            using (var form = new Form())
            {
                PictureBox pictureBox = new PictureBox();
                pictureBox.Dock = DockStyle.Fill;
                pictureBox.Image = Image.FromFile(filename);
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                form.Controls.Add(pictureBox);
                form.ShowDialog();
                pictureBox.Image.Dispose();
            }
            File.Delete(filename);
        }

        public static void DisplayWeather(CompleteWeather w)
        {
            var form = new WeatherForm(w);
            form.ShowDialog();
        }
    }
}
