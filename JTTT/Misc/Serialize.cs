﻿using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace JTTT
{
    static class Serialize
    {

        public static void Serialization(BindingList<ConditionTask> ConditionTaskList, string SerializeFileName)
        {

            FileStream fs = new FileStream(SerializeFileName, FileMode.Create);
            BinaryFormatter matter = new BinaryFormatter();

            try
            {
                matter.Serialize(fs, ConditionTaskList);
            }
            catch (SerializationException e)
            {
                MessageBox.Show("Serious Serialization Nope");
                Logger.Log(e.ToString());
                throw;

            }
            finally
            {
                fs.Close();
            }
        }

        public static BindingList<ConditionTask> Deserialization(string FileName)
        {
            FileStream fs = new FileStream(FileName,FileMode.Open);

            try
            {
                BinaryFormatter matter = new BinaryFormatter();
                BindingList<ConditionTask> tmp = default(BindingList<ConditionTask>); 
                return tmp = (BindingList<ConditionTask>)matter.Deserialize(fs);

            }
            catch (SerializationException e)
            {

                MessageBox.Show("Serious Deserialization Nope");
                Logger.Log(e.ToString());
                throw;
            }
            finally
            {
                fs.Close();
            }
            
        }

    }
}
